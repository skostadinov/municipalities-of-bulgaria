# Municipalities of Bulgaria

This states library offers easy accessible JS Object or Array formats of the Bulgaria Municipalities Names.

## Install

The installation is pretty straight forward
### npm

    npm i municipalities-of-bulgaria

### yarn

    yarn add municipalities-of-bulgaria

## Usage
### JS Object

    var municipalitiesOfBulgaria = require("municipalities-of-bulgaria");
    

### Array

    var municipalitiesOfBulgaria = require("municipalities-of-bulgaria");
    
